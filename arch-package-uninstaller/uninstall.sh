#!/bin/bash

# Author:     Muhammed Salih
# Date:       25/05/2020
# Version:    0.1.0

# Uninstall a package save the list of packages involved in its transaction
# Usage: Run sudo ./uninstall PACKAGE_NAME

if [ "$1" == "" ] 
then
    echo "You have not provided the name of package to uninstall"
    echo "Usage: Run sudo ./uninstall PACKAGE_NAME"
fi
date="$(date +'%d-%m-%y:%H:%M:%S%z')"
transaction_tree_file="$1_depender_tree_$date.txt"
transaction_list_file="$1_depender_list_$date.txt"
pactree -r $1 > "$transaction_tree_file"
cat "$transaction_tree_file" | sed -E 's/[├└─]/ /g' | sed -E 's/\ //g' > "$transaction_list_file"

transaction=($(cat "$transaction_list_file"))
echo Uninstalling ${transaction[*]}
pacman -R ${transaction[*]}
