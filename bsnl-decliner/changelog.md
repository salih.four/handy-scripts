## Version 0.1.1
### Bug fix
- Terminate script after decline is successful
### Feature
- Timestamp logs
### Additional Requirements
- winston npm package