#!/home/salih/.nvm/versions/node/v12.16.1/bin/node
// Please replace the above path to your node executable's path

// Decline bsnl broadband's daily topup advert redirect. Script is supposed to be run daily.

// Author:         Muhammed Salih
// Date:           23-05-2020
// Version:        0.1.1

// Requirements:
//     chromium browser and corresponding chromedriver
//     selenium webdriver npm package
//     winston npm package for logging

const {Builder, By, Key, until} = require("selenium-webdriver");
const winston = require("winston")

let level = "debug"
if (process.argv.includes("--debug")) {
    level = "debug"
}
const logger = winston.createLogger({
    level,
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.prettyPrint()
    ),
    transports: [
      new winston.transports.Console(),
    ]
});

(async function main(){
    debug = process.argv.includes("--debug") ? true : false;
    annoying_redirect_url = "http://172.30.110.25:8090/ssssportal/dailyFupPlanRedirection.do"
    
    try {
        let driver = new Builder().forBrowser('chrome').build();
        await driver.get(annoying_redirect_url);
        
        logger.info("Page loaded, pressing decline button")
        await driver.findElement(By.id('btnDailyDecline')).sendKeys(Key.RETURN, Key.RETURN);
        
        logger.info("Awaiting success message")
        let dailyDeclineClickDiv = await driver.findElement(By.id('dailyDeclineClickDiv'));
        await driver.wait(until.elementIsVisible(dailyDeclineClickDiv));
        
        logger.info("Decline completed");
    } catch (e) {
        logger.error("Error occured");
        logger.debug(e);
    }
})();
