- [ ] Detect if plan is already declined (check for #dailyDeclineDiv division visibility)  
  Currently, an element not interactable error is obtained if plan is already declined
- [ ] Detect if daily limit is not exceeded
  Currently, an error is produced when script is run before bsnl's daily limit is exceeded
- [ ] Terminate chromium after before exiting
