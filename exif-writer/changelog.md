## Version 0.1.0
### New Features
- Label png files in pwd with hardcoded author and licence property
- Label png files in pwd with dynamically obtained system time as creation time 

## Version 0.1.1
### Enhancements
- Author and Licence labelling now supports mp4 files