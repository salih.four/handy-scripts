#!/bin/bash

# Author:     Muhammed Salih
#              (gitlab.com/salih.four)
# Date:       30-04-2020
# Version:    0.1.1

# Dependencies: exiftool

# This script appends specified metadata to png and jpg files in the present working directory

# Metadata 
Author="Muhammed Salih"
CreationTime="$(date +"%Y:%m:%d %H:%M:%S%:z")" # Takes system's date and time
Copyright="CC Attribution-ShareAlike http://creativecommons.org/licenses/by-sa/4.0/"
echo $Author
echo $CreationTime
echo $Copyright
exiftool -overwrite_original_in_place -"Author"="$Author" -"CreationTime"="$CreationTime" -"Copyright"="$Copyright" *.png *.jpeg *.PNG *.JPEG *.mp4 *.MP4
