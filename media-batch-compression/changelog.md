## v0.1.1
### Bug Fix
- Resolved bug of video output format being mov instead of mp4
- Resolved bug occuring when file names contain spaces
## Version 0.1.0
### Features
- Resize large jpg images to 1920x1080 (Assuming landscape orientation, portrait images are resized to 1080x1920)
- Deshake and convert MOV video to mp4
