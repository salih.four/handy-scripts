#!/bin/bash

# Batch media compression script

# Author:         Muhammed Salih
# Date:           24/05/2020
# Version:        0.1.1

# Resize jpg images in the present directory
# Convert and deshake mov videos to mp4
# Save outputs to ./output

ls > .files_to_process.txt
mkdir output
IFS_BACKUP="$IFS"
IFS=$'\n'
files=($(cat .files_to_process.txt))
for file in ${files[*]}; do
    echo $file
    mime="$(mimetype -b $file)"
    mime0="$(echo "$mime" | sed -E 's/(.*)?\/.*/\1/')"
    filename=$(echo "$file" | sed -E 's/(.*)\.(\w*)$/\1/')
    extension=$(echo "$file" | sed -E 's/(.*)\.(\w*)$/\2/')
    if [ "$mime" == 'image/jpeg' ] 
    then
        IFS_BACKUP="$IFS"
        IFS=x
        dims=($(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$file"))
        IFS="$IFS_BACKUP"
        echo processing $file $mime ${dims[0]} ${dims[1]}
        if [ ${dims[0]} -gt ${dims[1]} ]
        then
            echo "orientation: landscape"
            convert "$file" -resize 1920x1080 "output/$file"
        else
            echo "orientation: portrait"
            convert "$file" -resize 1080x1920 "output/$file"
        fi

    echo $mime0
    elif [ "$mime0" == 'video' ]
    then
        echo processing $file $mime
        ffmpeg -i "$file"  -vcodec h264 -acodec mp2 -vf deshake "output/$filename.mp4"
    fi
done
IFS="$IFS_BACKUP"
rm .files_to_process.txt
